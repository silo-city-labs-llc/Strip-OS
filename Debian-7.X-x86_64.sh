#!/bin/bash 
# File:        Debian-7.X-x86_64.sh

echo "UPDATE device"
apt-get update -y -qq
apt-get upgrade -y -qq

echo "REMOVE non critical debian stuff"
#https://wiki.debian.org/ReduceDebianecho "REMOVE_non-critical_packages
apt-get remove -y -qq traceroute usbutils aptitude dictionaries-common aptitude-common
apt-get remove -y -qq iamerican ibritish laptop-detect vim-tiny vim-common wamerican info 
apt-get remove -y -qq keyboard-configuration install-info dictionaries-common

echo "REMOVE all python"
apt-get remove -y -qq `dpkg --get-selections | grep -v "deinstall" | grep python | sed s/install//`

echo "REMOVE all development packages (packages ending in "-dev")"
apt-get remove -y -qq `dpkg --get-selections | grep "\-dev" | sed s/deinstall// | sed s/install//`

read -p "REMOVE all sound support, optional, LXDE depends on it? " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    apt-get remove -y -qq `dpkg --get-selections | grep -v "deinstall" | grep sound | sed s/install//`
fi

read -p "REMOVE the GUI? " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    apt-get remove -y -qq libx11-.*
    apt-get remove -y -qq lxde lxappearance lxde-common lxde-core lxde-icon-theme lxinput lxmenu-data lxpanel lxpolkit lxrandr lxsession lxsession lxshortcut lxtask lxterminal obconf openbox desktop-base lightdm
fi

echo "REMOVE manpages, optional"
apt-get remove -y -qq manpages man-db
rm -fr /usr/share/doc/* /usr/share/man/*
rm -rf /usr/share/man/??
rm -rf /usr/share/man/??_*

echo "REMOVE IPv6 files"
rm /lib/xtables/libip6t_ah.so /lib/xtables/libip6t_dst.so /lib/xtables/libip6t_eui64.so /lib/xtables/libip6t_frag.so /lib/xtables/libip6t_hbh.so /lib/xtables/libip6t_hl.so /lib/xtables/libip6t_HL.so /lib/xtables/libip6t_icmp6.so /lib/xtables/libip6t_ipv6header.so /lib/xtables/libip6t_LOG.so /lib/xtables/libip6t_mh.so /lib/xtables/libip6t_REJECT.so  /lib/xtables/libip6t_rt.so
echo net.ipv6.conf.all.disable_ipv6=1 > /etc/sysctl.d/disableipv6.conf

echo "REMOVE unused kernels"
apt-get remove -y -qq $(dpkg -l|egrep '^ii  linux-(im|he)'|awk '{print $2}'|grep -v `uname -r`)
update-grub

echo "REMOVE orphaned files"
apt-get install -y -qq deborphan
deborphan | xargs apt-get -y -qq remove
deborphan --guess-dev | xargs apt-get -y -qq remove
apt-get purge -y -qq deborphan

echo "INSTALL needed programs"
apt-get install -y -qq sudo

echo "SETUP disable motd"
touch ~/.hushlogin

read -p "REMOVE all copyright files? " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    rm /usr/share/doc/*/copyright*
fi

read -p "REMOVE hashtag comments from all config files? " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    find / -iname *.conf |
    while read filename
    do
        echo "Cleaning $filename ..."
        sed '/^\#/d' $filename > /tmp/fbeyvbydfv
        mv /tmp/fbeyvbydfv $filename
    done
fi

echo "REMOVE Uneeded docs"
find /usr/share/doc -depth -type f |xargs rm -f || true
find /usr/share/doc -empty|xargs rmdir || true
rm -rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian > /usr/share/linda /var/cache/man

echo "REMOVE Uneeded locale files"
find /usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en' |xargs rm -rf

echo "REMOVE uneeded files by search"
find / \( -iname \*readme\* -o -iname \*credits\* -o -iname \*changelog\* -o -iname \*history.txt\* -o -iname \*help.txt\* -o -iname \*license\* -o -iname \*news.txt\* -o -iname \*todo\* \) |
while read filename
do
    echo "Deleting $filename ..."
    rm -f $filename
done

echo "REMOVE Uneeded Files by hand"
rm -f /usr/share/pixmaps/debian-logo.png
rm -rf /usr/share/apache2/icons/*

echo "CLEANUP old logs"
find /var/log -type f -name "*.gz" -delete
find /var/log -type f -name "*.tar.gz" -delete
find /var/log -type f -name "*.tgz" -delete
cat /dev/null > /var/log/syslog

echo "CLEANUP"
rm -f Debian-7.X-x86_64.sh
apt-get autoremove -y -qq
apt-get purge -y $(dpkg --list |grep '^rc' |awk '{print $2}')
apt-get clean
history -c

reboot
