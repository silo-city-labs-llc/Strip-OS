#!/bin/bash 
# File:        Ubuntu-14.04LTS-ARM.sh
# Description: Cleanup script for a UTILITE ubuntu installation using ARM, Run in root NOT sudo

echo "UPDATE device"
apt-get update -y -qq
apt-get upgrade -y -qq

echo "REMOVE non critical debian stuff"
apt-get remove -y -qq firefox gwibber remmina-common xul-ext-ubufox transmission-gtk thunderbird brasero totem rhythmbox gnome-media rhythmbox-ubuntuone guvcview onboard gnome-orca gcalctool deja-dup ubuntuone-installer aisleriot mahjongg gnomine evince eog shotwell simple-scan empathy
apt-get remove -y -qq netcat-openbsd netcat-traditional luajit scratch squeak-vm penguinspuzzle netsurf-gtk traceroute usbutils omxplayer liblapack3 libatlas3-base gdb aptitude
apt-get remove -y -qq libraspberrypi-doc raspberrypi-artwork
apt-get remove -y -qq wolfram-engine smbclient libsmbclient samba-common nfs-common dictionaries-common omxplayer scratch aptitude aptitude-common pcmanfm

echo "REMOVE all python"
apt-get remove -y -qq `dpkg --get-selections | grep -v "deinstall" | grep python | sed s/install//`

echo "REMOVE all development packages (packages ending in "-dev")"
apt-get remove -y -qq `dpkg --get-selections | grep "\-dev" | sed s/deinstall// | sed s/install//`

echo "REMOVE all sound support, optional, LXDE depends on it"
apt-get remove -y -qq `dpkg --get-selections | grep -v "deinstall" | grep sound | sed s/install//`

echo "REMOVE LXDE but not X11. You can still x-forward over SSH"
apt-get remove -y -qq lxde lxappearance lxde-common lxde-core lxde-icon-theme lxinput lxmenu-data lxpanel lxpolkit lxrandr lxsession lxsession lxshortcut lxtask lxterminal obconf openbox desktop-base lightdm

echo "REMOVE GUI"
#apt-get remove -y -qq task-desktop
#apt-get purge -y -qq  `sudo dpkg --get-selections | grep -v "deinstall" | grep x11 | sed s/install//`
#apt-get purge -y -qq  libx11-.*
#apt-get purge -y -qq  consolekit desktop-base* desktop-file-utils* gnome-icon-theme* gnome-themes-standard* hicolor-icon-theme* leafpad* midori* xserver-common* xserver-xorg* xserver-xorg-core* xserver-xorg-input-all* xserver-xorg-input-evdev* xserver-xorg-input-synaptics* xserver-xorg-video-fbdev*

echo "kill and delete utilite user"
pkill -KILL -u utilite
userdel utilite
rm -rf /home/utilite

echo "REMOVE manpages, optional"
apt-get remove -y -qq manpages man-db
rm -fr /usr/share/doc/* /usr/share/man/*
rm -rf /usr/share/man/??
rm -rf /usr/share/man/??_*

echo "CLEANUP old logs"
find /var/log -type f -name "*.gz" -delete
find /var/log -type f -name "*.tar.gz" -delete
find /var/log -type f -name "*.tgz" -delete
cat /dev/null > /var/log/syslog

echo "REMOVE copyright files"
rm /usr/share/doc/*/copyright*

echo "REMOVE Comments from config"
find / -iname *.conf |
while read filename
do
    echo "Cleaning $filename ..."
    sed '/^\#/d' $filename > /tmp/fbeyvbydfv
    mv /tmp/fbeyvbydfv $filename
done

echo "REMOVE Uneeded docs"
find /usr/share/doc -depth -type f |xargs rm -f || true
find /usr/share/doc -empty|xargs rmdir || true
rm -rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian > /usr/share/linda /var/cache/man

echo "REMOVE unused compilers"
apt-get remove -y -qq gcc-4.4-base:armhf gcc-4.5-base:armhf gcc-4.6-base:armhf

echo "REMOVE Uneeded locale files"
find /usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en' |xargs rm -rf

echo "REMOVE uneeded files by search"
find / \( -iname \*readme\* -o -iname \*credits\* -o -iname \*changelog\* -o -iname \*history.txt\* -o -iname \*help.txt\* -o -iname \*license\* -o -iname \*news.txt\* -o -iname \*todo\* \) |
while read filename
do
    echo "Deleting $filename ..."
    rm -f $filename
done

echo "REMOVE Uneeded Files by hand"
rm -f /usr/share/pixmaps/debian-logo.png
rm -rf /usr/share/apache2/icons/*
rm -f /etc/sudoers.d/README

echo "REMOVE IPv6 files"
sudo rm /lib/xtables/libip6t_ah.so /lib/xtables/libip6t_dst.so /lib/xtables/libip6t_eui64.so /lib/xtables/libip6t_frag.so /lib/xtables/libip6t_hbh.so /lib/xtables/libip6t_hl.so /lib/xtables/libip6t_HL.so /lib/xtables/libip6t_icmp6.so /lib/xtables/libip6t_ipv6header.so /lib/xtables/libip6t_LOG.so /lib/xtables/libip6t_mh.so /lib/xtables/libip6t_REJECT.so  /lib/xtables/libip6t_rt.so
echo net.ipv6.conf.all.disable_ipv6=1 > /etc/sysctl.d/disableipv6.conf

echo "REMOVE unused kernels"
apt-get remove -y -qq $(dpkg -l|egrep '^ii  linux-(im|he)'|awk '{print $2}'|grep -v `uname -r`)
update-grub

echo "SETUP disable motd"
touch ~/.hushlogin

echo "CLEANUP"
rm -f Ubuntu-14.04LTS-UTILITE-ARM.sh
apt-get autoremove -y -qq
apt-get purge -y $(dpkg --list |grep '^rc' |awk '{print $2}')
apt-get clean
history -c

reboot
